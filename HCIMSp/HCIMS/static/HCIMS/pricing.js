exports.getRecentDeviceTemps = function(req, res) {
  // Make the call to MongoDB to grab the relevant records
  req.db.tempReadings.find({ deviceId: req.params.id }, null,
    { limit: 120, sort: { 'time': -1 }}).toArray(function(err, results) {
    if(err) { next(err); }
 
    res.format({
      // Respond to normal browser requests with the 404 page
      html: function() {
        res.render('404', {
          title: 'Page not found'
        });
      },
 
      // Respond to AJAX requests with Morris-consumable data
      json: function() {
        // Initialise an array for returning later
        var graphData = [];
 
        // Go through each result
        for(var i = 0; i &lt; results.length; i++) {
          var result = results[i];
          // Check these results are valid
          if(result.times &amp;&amp; result.temp) {
            var date = new Date(result.time);
            var temp = result.temp;
 
            // Create an object for Morris.js to read
            var graphPoint = {};
            graphPoint.timestamp = date.getTime();
            graphPoint.temp = temp;
 
            // Push the object to the array for returning
            graphData.push(graphPoint);
          }
        }
 
        // Return the graphData object as JSON
        res.json({ graphData: graphData });
      }
    });
  });
};
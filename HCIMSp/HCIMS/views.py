# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from users.models import Doctor
from newsapi.newsapi_client import NewsApiClient
import json
import requests

@login_required
def index(request):
    doctor = Doctor.objects.all()
    # apikey = '667065c36c064053be40c247f1b195e0'
    context = {
        'doctor':doctor
    }
    return render(request, 'HCIMS/index.html',context)
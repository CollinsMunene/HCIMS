# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Profile, Patient, History, Doctor, User

# Register your models here.
admin.site.site_header = 'Administration'
admin.site.register(User)
admin.site.register(Profile)
admin.site.register(Patient)
admin.site.register(History)

class DoctorAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'phone_number')

admin.site.register(Doctor, DoctorAdmin)
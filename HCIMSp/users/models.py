# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
# from django.contrib.auth.models import User
from PIL import Image
import datetime
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.

class User(AbstractUser):
    is_patient = models.BooleanField('patient status', default=False)
    is_doctor = models.BooleanField('doctor status', default=False)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics', null=True)
        
    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)

class Patient(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    email = models.EmailField(max_length=30, default="yours@gmail.com")
    phone_number = PhoneNumberField()
    gender = models.CharField(max_length=1, choices=(('M', 'Male'), ('F', 'Female')), blank=True)
    allergy = models.CharField(max_length=180, default="yours@gmail.com")
    blood_type = models.CharField(max_length=3, default="yours@gmail.com")
    height = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    blood_pressure = models.IntegerField(default=0)
    bmi_levels = models.IntegerField(default=0)
    temperature = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username
    
    def save(self, *args, **kwargs):
        super(Patient, self).save(*args, **kwargs)

class Doctor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics', null=True)
    email = models.EmailField(max_length=30, default="yours@gmail.com")
    phone_number = models.IntegerField(default=0)
    gender = models.CharField(max_length=1, choices=(('M', 'Male'), ('F', 'Female')), blank=True)
    specialization = models.CharField(max_length=60, default="doctor")

    def __str__(self):
        return self.user.username

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)
        
class History(models.Model):
    patient_history = models.ForeignKey(to=Patient, null=True, blank=True,on_delete=models.CASCADE)
    user = models.CharField(max_length=180)
    date = models.DateField(("Date"), default=datetime.date.today)
    time = models.TimeField(auto_now=False, auto_now_add=False, null=True)
    doctor = models.ForeignKey(to=Doctor, null=True, blank=True,on_delete=models.CASCADE)
    tests = models.CharField(max_length=360, default='NONE')
    drugs = models.CharField(max_length=360, default='none')
    consultation_cost = models.IntegerField(default=0)
    drugs_cost = models.IntegerField(default=0)
    labs_cost = models.IntegerField(default=0)
    total_cost = models.IntegerField(default=0)

    def __str__(self):
        return self.patient_history.user.username
from django.contrib.auth.decorators import user_passes_test
def group_required(*args):
    if args:
        def decorator(user):
            if user.is_doctor or user.is_superuser:
                return True
            return user.is_authenticated() and user.groups.filter(name__in=args).exists()
    else:
        decorator = lambda x: x.is_authenticated()

    return user_passes_test(decorator)
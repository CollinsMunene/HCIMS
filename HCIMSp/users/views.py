# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.decorators import login_required
from .models import Patient, History, Doctor
from rest_framework import viewsets
from .serializers import HistorySerializer
import requests
from .forms import ContactForm, PatientsignupForm, DoctorsignupForm
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from .decorators import group_required
# Create your views here.

# def register(request):
#     if request.method == 'POST':
#         form = UserRegisterForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get('username')
#             # messages.success(request, 'Account created!! Now log in')
#             return redirect('login')
#     else:
#         form = UserRegisterForm()
#     return render(request, 'users/register.html',{'form': form})
    
def patientsignup(request):
    if request.method == 'POST':
        form = PatientsignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            
            def get_context_data(self, **kwargs):
                kwargs['user_type'] = 'patient'
                return super().get_context_data(**kwargs)
            messages.success(request, 'Account created!! Now log in')
            return redirect('login')
    else:
        messages.success(request, 'Error')
        form = PatientsignupForm()
    return render(request, 'users/patientregister.html',{'form': form})

def doctorsignup(request):
    if request.method == 'POST':
        form = DoctorsignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')

            def get_context_data(self,**kwargs):
                kwargs['user_type'] = 'doctor'
                return super().get_context_data(**kwargs)
            # messages.success(request, 'Account created!! Now log in')
            return redirect('login')
    else:
        messages.success(request, 'Error')
        form = DoctorsignupForm()
    return render(request, 'users/doctorregister.html',{'form': form})

@login_required
def profile(request):
    patients = Patient.objects.all()
    patient_filter = patients.filter(user=request.user)
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, 'Account updated, now login')
            return redirect('profile')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
        
    context = {
        'u_form': u_form,
        'p_form': p_form,
        'filter': patient_filter,
    }
    return render(request, 'users/profile.html', context)

@login_required
def history(request):
    history = History.objects.all()
    history_filter = history.filter(user=request.user)
    history_docfilter = Doctor.objects.filter(history__user=request.user)
    context = {
        'history_filter': history_filter,
        'history_docfilter':history_docfilter
    }
    return render(request, 'HCIMS/history.html', context)

@login_required
def pricing(request):
    price = History.objects.all()
    price_filter = price.filter(user=request.user)
    context = {
        'price_filter': price_filter,
    }
    return render(request, 'HCIMS/pricing.html', context)

class HistoryViewSet(viewsets.ModelViewSet):
    queryset = History.objects.all().order_by('user')
    serializer_class = HistorySerializer
    def get_queryset(self):
        if self.action == 'list':
            return self.queryset.filter(user=self.request.user)
        return self.queryset

@login_required
@group_required('doctor')
def doctor(request):
    doctor = Doctor.objects.all()
    doctor_filter = doctor.filter(user=request.user)
    history = History.objects.filter(doctor__user=request.user)
    patient = Patient.objects.all()
    context = {
        'doctor_filter': doctor_filter,
        'history': history,
        'patient': patient
    }
    return render(request, 'HCIMS/doctor.html', context)

def emailView(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['collinshillary1@gmail.com'], fail_silently=False)
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('success')
    return render(request, "HCIMS/contact.html", {'form': form})

def successView(request):
    return HttpResponse('Success! Thank you,,Your appointment request has been sent, You will receive a feedback within 6 hours.')

# def feedView(request):
#     # Init
#     newsapi = NewsApiClient(api_key='667065c36c064053be40c247f1b195e0')

#     # /v2/top-headlines
#     top_headlines = newsapi.get_top_headlines(q='bitcoin',
#                                             sources='bbc-news,the-verge',
#                                             category='business',
#                                             language='en',
#                                             country='us')

#     # /v2/everything
#     all_articles = newsapi.get_everything(q='bitcoin',
#                                         sources='bbc-news,the-verge',
#                                         domains='bbc.co.uk,techcrunch.com',
#                                         from_param='2017-12-01',
#                                         to='2017-12-12',
#                                         language='en',
#                                         sort_by='relevancy',
#                                         page=2)

#     # /v2/sources
#     sources = newsapi.get_sources()

#     context = {
#         'all_articles': all_articles,
#         'top_headlines': top_headlines
#     }
#     return render(request, 'HCIMS/index.html', context)
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Profile, Patient, User

@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
        if created:
                Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
        instance.profile.save()

# @receiver(post_save, sender=User)
# def create_patient(sender, instance, created, **kwargs):
#         if created:
#                 Patient.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_patient(sender, instance, **kwargs):
#         instance.patient.save()
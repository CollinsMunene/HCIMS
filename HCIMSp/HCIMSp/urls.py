from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib import admin
from users import views
from django.conf import settings
from django.conf.urls.static import static
from HCIMS import views as hci_views
from django.conf.urls import include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', hci_views.index, name='home'),
    url(r'^patientsignup/', views.patientsignup, name='patientsignup'),
    url(r'^doctorsignup/', views.doctorsignup, name='doctorsignup'),
    url(r'^history/', views.history, name='history'),
    # url(r'^register/', views.register, name='register'),
    url(r'^profile/', views.profile, name='profile'),
    url(r'^login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    url(r'^logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    url(r'^history/', views.history, name='history'),
    url(r'^pricing/', views.pricing, name='pricing'),
    url(r'^doctor/', views.doctor, name='Mypatients'),
    url(r'^contact/', views.emailView, name='contact'),
    url(r'^success/', views.successView, name='success'),
    url(r'^api/', include('users.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

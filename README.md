# HOSPITAL CLIENT INFORMATION MANAGEMENT SYSTEM

The HCIMS is a system that was developed to easily integrate with the current Hospital Information System(HIS).

Developed using Django and other web development technologies like HTML, CSS, JavaScript,Bootstrap and some functionality with Chart.js. It makes it easy for patients to view their medical history and be able to keep track of their spending at the hospital, this brings about transparency between the hospital and it's patients.

HCIMS is designed to work with an already existing database, but for this project we are creating our own database basically an sqLite just to mimick the actual database on production.

## Active Developers

[Collins Munene](https://github.com/CollinsMunene)

## Beta Testers / Bug Fixers

[Nick Mwangemi](https://github.com/nickmwangemi)

[Restercuter](https://github.com/Restercuter)

## SYSTEM FEATURES

### As a patient, you will be able to:

1. View Hospital top Doctors
2. View hospital Announcements and Important Details
3. View previous Hospital visits(hospital history)
4. View the total amount of money used on every visit
5. View a breakdown of the total amount of money used on every visit
6. Make Appointments easily

### As a doctor, you will be able to:

1. View Patient history easily, this includes:

- View drugs prescribed previously
- View date and time when the patient visited the hospital
- View who treated the patient previously

### As a hospital, you will be able to:

1. Offer Transparency to your patients.
2. Be able to track and have logs incase an issue arises.

### Getting Started

1. Clone the repo:

```
git clone https://github.com/CollinsMunene/HCIMS.git
```

2. Setup your local environment.

```
cd HCIMS
source HCIMS_environment/bin/activate
pip3 install -r ./requirements.txt
cd HCIMSp
python3 manage.py runserver
```

Thats all you should be up and running

3. If you want to access the admin panel at http://0.0.0.0:8000/admin
   create a superuser on the terminal using the command below:

```
python3 manage.py createsuperuser
```

## TODO

1. Improve the interface
2. Add private chat
3. Add automatic appointment scheduler
